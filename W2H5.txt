Cecilia Michell

Soal 1 Membuat Database
Buatlah database dengan nama myshop
-> create database myshop;

Soal 2 Membuat Table di dalam Database
MariaDB [myshop]> create table users(
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

MariaDB [myshop]> create table categories(
    -> id int auto_increment primary key,
    -> name varchar(255));

MariaDB [myshop]> create table items(
    -> id int auto_increment primary key,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int references category(id)
    -> );

Soal 3 Memasukkan Data pada Table
MariaDB [myshop]> insert into users(name, email, password) values("John Doe", "John@doe.com", "john123"), ("Jane Doe", "jane@doe.com", "jenita123");

MariaDB [myshop]> insert into categories(name) values("gadget"), ("cloth"), ("men"), ("women"), ("branded");

MariaDB [myshop]> insert into items(name, description, price, stock, category_id) values("Sumsang B50", "hape keren dari merek sumsang", 40000000, 100, 1), ("Uniklooh", "baju keren dari brand ternama", 5000000, 50, 2), ("IMHO Watch", "jam tangan anak yang jujur banget", 20000000, 10, 1);

Soal 4 Mengambil Data dari Database
a. Mengambil data users (kecuali password)
	MariaDB [myshop]> select id, email from users;

b. Mangambil data items
MariaDB [myshop]> select * from items where price > 1000000;
MariaDB [myshop]> select * from items where name like '%sang%';

c. Menampilkan data items join dengan kategori
MariaDB [myshop]> select items.name, items.description, items.price, items.stock, items.category_id, categories.name as ketegori from items inner join categories on items.category_id = categories.id;

Soal 5 Mengubah Data dari Database
update items set price = 2500000 where name = 'Sumsang B50';